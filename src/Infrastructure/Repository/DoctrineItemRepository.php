<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Item;
use App\Domain\EntityCollection\ItemCollection;
use App\Domain\Repository\ItemRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;

class DoctrineItemRepository implements ItemRepositoryInterface
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function list(int $offset, int $count): ItemCollection
    {
        $items = $this->entityManager->getRepository(Item::class)->findBy([], null, $count, $offset);

        return new ItemCollection($items);
    }

    public function get(int $id): Item
    {
        $item = $this->entityManager->getRepository(Item::class)->find($id);

        if(!$item) {
            throw new \Exception(
                'Item not found in catalog'
            );
        }

        return $item;
    }

    public function save(Item $item): void
    {
        $this->entityManager->persist($item);

        try {
            $this->entityManager->flush();
        } catch (OptimisticLockException $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

    }

}