<?php

namespace App\Infrastructure\Controller;

use App\Application\UseCase\CreateItem\CreateItemRequest;
use App\Application\UseCase\CreateItem\CreateItemUseCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class CreateItemController
{
    private $createItemUseCase;

    private $request;

    public function __construct(CreateItemUseCase $createItemUseCase, RequestStack $request)
    {
        $this->createItemUseCase = $createItemUseCase;
        $this->request = $request->getCurrentRequest();
    }

    public function doExecute()
    {
        $this->createItemUseCase->execute(
            new CreateItemRequest(
                $this->request->get('id'),
                $this->request->get('image'),
                $this->request->get('title'),
                $this->request->get('author'),
                $this->request->get('price')
            )
        );

        return new JsonResponse("Item created successfully");
    }
}