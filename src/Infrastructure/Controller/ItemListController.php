<?php

namespace App\Infrastructure\Controller;

use App\Domain\Repository\ItemRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ItemListController
{
    private $doctrineItemRepository;

    public function __construct(ItemRepositoryInterface $doctrineItemRepository)
    {
        $this->doctrineItemRepository = $doctrineItemRepository;
    }

    public function doExecute($offset = 0, $count = 20)
    {
        $items = $this->doctrineItemRepository->list($offset, $count);

        return new JsonResponse($items->toArray());
    }
}