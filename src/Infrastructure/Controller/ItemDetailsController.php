<?php

namespace App\Infrastructure\Controller;

use App\Application\UseCase\GetItemDetails\GetItemDetailsRequest;
use App\Application\UseCase\GetItemDetails\GetItemDetailsUseCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class ItemDetailsController
{
    private $getItemDetailsUseCase;

    private $request;

    public function __construct(GetItemDetailsUseCase $getItemDetailsUseCase, RequestStack $request)
    {
        $this->getItemDetailsUseCase = $getItemDetailsUseCase;
        $this->request = $request->getCurrentRequest();
    }

    /**
     * @return JsonResponse
     */
    public function doExecute()
    {
        $id = $this->request->get('id');

        $itemResponse = $this->getItemDetailsUseCase->execute(
            new GetItemDetailsRequest($id)
        );

        return new JsonResponse($itemResponse->getItem());
    }
}