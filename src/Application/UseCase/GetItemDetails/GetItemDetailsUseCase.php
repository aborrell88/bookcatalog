<?php

namespace App\Application\UseCase\GetItemDetails;

use App\Domain\Repository\ItemRepositoryInterface;

class GetItemDetailsUseCase
{
    private $eloquentItemRepository;

    public function __construct(ItemRepositoryInterface $eloquentItemRepository)
    {
        $this->eloquentItemRepository = $eloquentItemRepository;
    }

    public function execute(GetItemDetailsRequest $request): ?GetItemDetailsResponse
    {
        $item = $this->eloquentItemRepository->get($request->getId());

        return new GetItemDetailsResponse($item->toArray());
    }

}