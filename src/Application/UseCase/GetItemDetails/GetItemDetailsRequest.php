<?php

namespace App\Application\UseCase\GetItemDetails;

class GetItemDetailsRequest
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}