<?php

namespace App\Application\UseCase\GetItemDetails;

class GetItemDetailsResponse
{
    private $item;

    public function __construct($item)
    {
        $this->item = $item;
    }

    public function getItem()
    {
        return $this->item;
    }

}