<?php

namespace App\Application\UseCase\CreateItem;

use App\Domain\Entity\Item;
use App\Domain\Repository\ItemRepositoryInterface;
use http\Env\Response;

class CreateItemUseCase
{
    private $doctrineItemRepository;

    public function __construct(ItemRepositoryInterface $doctrineItemRepository)
    {
        $this->doctrineItemRepository = $doctrineItemRepository;
    }

    public function execute(CreateItemRequest $request): ?Response
    {
        $item = new Item(
            ($request->getId()) ?? $request->getId(),
            $request->getImage(),
            $request->getTitle(),
            $request->getAuthor(),
            (float) $request->getPrice()
        );

        $this->doctrineItemRepository->save($item);

        return null;
    }
}