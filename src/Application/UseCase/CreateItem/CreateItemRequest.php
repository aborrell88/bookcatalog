<?php

namespace App\Application\UseCase\CreateItem;

class CreateItemRequest
{
    private $id;

    private $image;

    private $title;

    private $author;

    private $price;

    public function __construct(int $id = null, string $image, string $title, string $author, float $price)
    {
        $this->id = $id;
        $this->image = $image;
        $this->title = $title;
        $this->author = $author;
        $this->price = $price;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

}