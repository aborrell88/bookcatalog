<?php

namespace App\Domain\Entity;

class Item
{
    private $id;

    private $image;

    private $title;

    private $author;

    private $price;

    public function __construct(int $id, string $image, string $title, string $author, float $price)
    {
        $this->id = $id;
        $this->image = $image;
        $this->title = $title;
        $this->author = $author;
        $this->price = $price;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'image' => $this->getImage(),
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'price' => $this->getPrice()
        ];
    }

}