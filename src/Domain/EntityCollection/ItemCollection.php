<?php

namespace App\Domain\EntityCollection;

use App\Domain\Entity\Item;

class ItemCollection
{
    /** @var Item[] */
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function toArray(): array
    {
        $items = [];

        foreach($this->items as $item) {
            $items[] = $item->toArray();
        }

        return $items;
    }

}