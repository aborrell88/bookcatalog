<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Item;
use App\Domain\EntityCollection\ItemCollection;

interface ItemRepositoryInterface
{
    public function list(int $offset, int $limit): ItemCollection;

    public function get(int $id): Item;

    public function save(Item $item): void;
}