<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'infrastructure.controller.item_list_controller' shared service.

include_once $this->targetDirs[3].'/src/Infrastructure/Controller/ItemListController.php';

return $this->services['infrastructure.controller.item_list_controller'] = new \App\Infrastructure\Controller\ItemListController(($this->privates['infrastructure.repository.doctrine_item_repository'] ?? $this->load(__DIR__.'/getInfrastructure_Repository_DoctrineItemRepositoryService.php')));
