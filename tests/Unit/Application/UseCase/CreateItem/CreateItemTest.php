<?php

namespace App\Tests\Unit\Application\UseCase\CreateItem;

use App\Domain\Repository\ItemRepositoryInterface;
use App\Application\UseCase\CreateItem\CreateItemUseCase;
use App\Application\UseCase\CreateItem\CreateItemRequest;
use App\Domain\Entity\Item;

use PHPUnit\Framework\TestCase;

class CreateItemTest extends TestCase
{
    public function test_save_method_on_items_repository() {

        // Having
        /** @var ItemRepositoryInterface|\PHPUnit_Framework_MockObject_MockObject $itemsRepository */
        $itemsRepository = $this->createMock(ItemRepositoryInterface::class);

        $createMeetingUseCase = new CreateItemUseCase($itemsRepository);

        $id = 6;
        $image = 'http://www.bookcatalog/images/img6.png';
        $title = 'Canción de Hielo y Fuego: Vientos de invierno';
        $author = 'G. R. R. Martin';
        $price = 29.95;
        $item = new Item($id, $image, $title, $author, $price);

        // Expect
        $itemsRepository->expects($this->once())
            ->method('save')
            ->with($item);

        // Act
        $createMeetingUseCase->execute(
            new CreateItemRequest($id, $image, $title, $author, $price)
        );

    }
}